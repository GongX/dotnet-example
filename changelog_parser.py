#!/bin/env python

import sys
import re

def parse_changelog(filename, version):
    pattern = re.compile(r'## {}(.+?)^## \d+\.\d+\.\d+\.\d+?'.format(re.escape(version)), flags=re.DOTALL|re.MULTILINE)
    with open(filename, 'r+') as f:
        text = f.read()
        m = re.search(pattern, text)
        result = m.group(1)
        stripped_string = result.lstrip().rstrip()
        return stripped_string

def main(filename, version):
    print(parse_changelog(filename, version))
    

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])